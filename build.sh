#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

# build instructions
#
pushd source || exit 1
mkdir build
cd build || exit 1
cmake \
	-DCMAKE_INSTALL_PREFIX=../../tmp \
	-DBuildJK2SPEngine=ON \
	-DBuildJK2SPGame=ON \
	-DBuildJK2SPRdVanilla=ON \
	..
make -j "$(nproc)"
make install
popd || exit 1

find tmp/

cp -v tmp/JediAcademy/OpenJK/cgamex86_64.so tmp/JediOutcast/OpenJK/
cp -v tmp/JediAcademy/OpenJK/uix86_64.so    tmp/JediOutcast/OpenJK/

cp -rfv tmp/JediAcademy/* "6020/dist/"
cp -rfv tmp/JediOutcast/* "6030/dist/"

find "6020"
find "6030"
